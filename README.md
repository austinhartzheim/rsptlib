# rsptlib
*Rust library supporting creation of Tor pluggable transports.*

This implementation is based heavily on the [Go pluggable transport library](https://gitweb.torproject.org/pluggable-transports/goptlib.git/), reimplementing many of the same functions and tests in Rust.

## Roadmap
Initial Roadmap:
- [x] Parsing/validation of environment variables.
- [x] Cookie ORPort authentication.
- [~] Parent process communication.
- [~] SOCKS protocol handling.
- [ ] Example pluggable transport.
- [ ] Documentation on writing pluggable transports.

Future work:
- [ ] Test harnesses for property testing, fuzzing, and performance testing.
- [ ] Refactor to idiomatic Rust and stabilize public interface.

## Developing
Assuming a [standard Rust installation](https://www.rust-lang.org/tools/install), the following commands can be run from inside the repository:

```sh
cargo check         # check syntax and validate types
cargo test          # execute unit tests
cargo doc --open    # render HTML documentation and open in a browser
```

## Code Layout
```
src/
  client/       # Support for creating pluggable transport clients.
  server/       # Support for creating pluggable transport servers.
  args.rs       # Args data structure, supporting client/server parameters.
  auth.rs       # Cookie-based authentication scheme.
  env.rs        # Environment variable parsing/validation.
  lib.rs        # Top-level module.
  messages.rs   # Parent process communication.
```

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

See `LICENSE.txt` for details.
