//! This crate implements the Tor pluggable transport specification.
//!
//! # Example Client
//! ```ignore
//! use rsptlib::client::ClientInfo;
//!
//! fn main() {
//!     let client = ClientInfo::with_parameters(vec!["testmethod".into()], None)
//!         .expect("Failed to initialize ClientInfo");
//!     if let Some(url) = client.proxy_url() {
//!         // Clients must interpret the proxy URL.
//!         // TODO: log whether proxy is unsupported.
//!         // TODO: exit with status code 1 if configured proxy is not supported
//!         unimplemented!();
//!     }
//!     for method in client.methods() {
//!         match method.as_str() {
//!             "testmethod" => {
//!                 unimplemented!();
//!             },
//!             _ => {
//!                 // TODO: log unsupported method
//!             }
//!         }
//!     }
//! }
//! ```

// Enable support for `SyncLazy`. This is only used in unit tests to synchronize access to
// environment variables. Stabilization of the `once_cell` feature can be tracked here:
// https://github.com/rust-lang/rust/issues/74465
#![feature(once_cell)]

#[cfg(test)]
#[macro_use]
extern crate maplit;

pub mod args;
mod auth;
pub mod client;
pub mod env;
pub mod messages;
pub mod server;
mod socks;
