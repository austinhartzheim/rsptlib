//! Provides the `ClientInfo` struct and other utilities for the implementation of pluggable
//! transport clients. This module calls out to other modules to read client configuration
//! from environment variables to ease input validation.
use crate::env;

use url::Url;

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum ClientInfoError {
    ClientTransportsError(env::ClientTransportsError),
    ProxyUrlError(env::ProxyUrlError),
    TransportVersionError(env::TransportVersionError),
}
impl From<env::ProxyUrlError> for ClientInfoError {
    fn from(err: env::ProxyUrlError) -> Self {
        Self::ProxyUrlError(err)
    }
}
impl From<env::TransportVersionError> for ClientInfoError {
    fn from(err: env::TransportVersionError) -> Self {
        Self::TransportVersionError(err)
    }
}
impl From<env::ClientTransportsError> for ClientInfoError {
    fn from(err: env::ClientTransportsError) -> Self {
        Self::ClientTransportsError(err)
    }
}

/// Read basic configuration from the environment and expose the parsed method names and proxy
/// via methods.
pub struct ClientInfo {
    method_names: Vec<String>,
    proxy_url: Option<url::Url>,
}

impl ClientInfo {
    /// Create an instance of `ClientInfo`, reading configuration from environment variables.
    ///
    /// This function validates the environment variables, checking for version compatibility and
    /// rejecting malformed parameters. If the configuration fails validation, return an error.
    pub fn new() -> Result<Self, ClientInfoError> {
        let _version = env::managed_transport_version()?;
        // TODO: log version

        let method_names = env::client_transports()?;
        let proxy_url = env::proxy_url()?;

        Self::with_parameters(method_names, proxy_url)
    }

    /// Create an instance of `ClientInfo` with the provided method names and optional proxy URL.
    ///
    /// This function is intended for use in tests and therefore does not check version
    /// compatibility. Use [`ClientInfo::new`] in production code to populate the configuration
    /// from environment variables.
    pub fn with_parameters(
        method_names: Vec<String>,
        proxy_url: Option<Url>,
    ) -> Result<Self, ClientInfoError> {
        if let Some(u) = &proxy_url {
            env::validate_proxy_url(&u)?;
        }

        Ok(ClientInfo {
            method_names,
            proxy_url,
        })
    }

    /// Return the methods read from the `TOR_PT_CLIENT_TRANSPORTS` environment variable.
    pub fn methods(&self) -> &[String] {
        self.method_names.as_slice()
    }

    /// If a proxy was configured via `TOR_PT_PROXY`, return the URL for the proxy.
    ///
    /// Implementations must check if a proxy has been configured and check if they support the
    /// requested proxy scheme.
    pub fn proxy_url(&self) -> &Option<url::Url> {
        &self.proxy_url
    }
}
