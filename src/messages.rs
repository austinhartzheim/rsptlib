//! Manages communications with the Tor process via stdout.
//!
//! Implements the message format described in `pt-spec.txt`, section 3.3.
use std::fmt::{self, Display, Write};
use std::net;

use crate::args;

pub fn log(message: &Messages) -> String {
    let log_line = message.to_string();
    if !is_log_safe(&log_line) {
        panic!("log contains forbidden bytes");
    }
    log_line
}

/// Returns `true` if and only if the provided string's characters are allowed in an output line.
/// <ArgChar> ::= <any US-ASCII character but NUL or NL>
fn is_log_safe(s: &str) -> bool {
    s.chars().all(|c| c.is_ascii() && c != '\x00' && c != '\n')
}

/// Inter-process communication messages.
///
/// Implements section 3.3 of the pluggable transport spec.
pub enum Messages<'a> {
    EnvError {
        msg: &'a str,
    },
    VersionError {
        msg: &'a str,
    },
    Version {
        version: &'a str,
    },
    CMethodError {
        method: &'a str,
        msg: &'a str,
    },
    SMethodError {
        method: &'a str,
        msg: &'a str,
    },
    ProxyError {
        msg: &'a str,
    },
    CMethod {
        name: &'a str,
        socks: SocksVersion,
        addr: net::SocketAddr,
    },
    CMethodsDone,
    SMethod {
        name: &'a str,
        addr: net::SocketAddr,
        args: Option<args::Args>,
    },
    SMethodsDone,
    ProxyDone,
    /// See `pt-spec.txt`, section 3.3.4.
    Log {
        severity: LogSeverity,
        msg: &'a str,
    },
}

impl<'a> Messages<'a> {
    fn fmt_args(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Messages::EnvError { msg } => f.write_str(msg),
            Messages::VersionError { msg } => f.write_str(msg),
            Messages::Version { version } => f.write_str(version),
            Messages::CMethodError { method, msg } => {
                f.write_str(method)?;
                f.write_char(' ')?;
                f.write_str(msg)
            }
            Messages::SMethodError { method, msg } => {
                f.write_str(method)?;
                f.write_char(' ')?;
                f.write_str(msg)
            }
            Messages::ProxyError { msg } => f.write_str(msg),
            Messages::CMethod { name, socks, addr } => {
                f.write_str(name)?;
                f.write_char(' ')?;
                socks.fmt(f)?;
                f.write_char(' ')?;
                addr.fmt(f)
            }
            Messages::CMethodsDone => f.write_str("DONE"),
            Messages::SMethod { name, addr, args } => {
                f.write_str(name)?;
                f.write_char(' ')?;
                addr.fmt(f)?;
                if let Some(args) = args {
                    f.write_str(" ARGS:")?;
                    f.write_str(&args.encode_smethod_args())?;
                }
                Ok(())
            }
            Messages::SMethodsDone => f.write_str("DONE"),
            Messages::ProxyDone => f.write_str("DONE"),
            Messages::Log { severity, msg } => {
                f.write_str("SEVERITY=")?;
                severity.fmt(f)?;
                f.write_char(' ')?;

                let escaped = encode_c_string(msg);
                f.write_str(&escaped)
            }
        }
    }

    fn keyword(&self) -> &'static str {
        match self {
            Messages::EnvError { .. } => "ENV-ERROR",
            Messages::VersionError { .. } => "VERSION-ERROR",
            Messages::CMethodError { .. } => "CMETHOD-ERROR",
            Messages::SMethodError { .. } => "SMETHOD-ERROR",
            Messages::ProxyError { .. } => "PROXY-ERROR",
            Messages::CMethod { .. } => "CMETHOD",
            Messages::CMethodsDone => "CMETHODS",
            Messages::SMethod { .. } => "SMETHOD",
            Messages::SMethodsDone => "SMETHODS",
            // TODO: The Go implementation appears to handle the `PROXY DONE` long message
            // differently than the others. Check that `PROXY` is the correct keyword.
            Messages::ProxyDone => todo!(),
            Messages::Version { .. } => "VERSION",
            Messages::Log { .. } => "LOG",
        }
    }
}

impl<'a> fmt::Display for Messages<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(self.keyword())?;
        f.write_char(' ')?;
        self.fmt_args(f)
    }
}

/// Log severity. `control-spec.txt`, section 4.1.5.
pub enum LogSeverity {
    Debug,
    Info,
    Notice,
    Warn,
    Err,
}

impl Display for LogSeverity {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Debug => f.write_str("DEBUG"),
            Self::Info => f.write_str("INFO"),
            Self::Notice => f.write_str("NOTICE"),
            Self::Warn => f.write_str("WARN"),
            Self::Err => f.write_str("ERR"),
        }
    }
}

/// Supported SOCKS protocol versions.
pub enum SocksVersion {
    Socks4,
    Socks5,
}

impl Display for SocksVersion {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Socks4 => f.write_str("socks4"),
            Self::Socks5 => f.write_str("socks5"),
        }
    }
}

/// Encode a string matching the rules implemented in the Go Pluggable Transport implementation.
///
/// These rules are based on:
/// * Section 2.1.1 in `control-spec.txt`.
/// * RFC 2822, section 3.2.5.
/// * https://bugs.torproject.org/29432
fn encode_c_string(s: &str) -> String {
    let bytes = s.as_bytes();
    let mut buf = String::with_capacity(bytes.len() + 2);

    buf.push('"');
    for b in s.as_bytes().iter() {
        match b {
            32 | 33 | 35..=91 | 93..=126 => buf.push(*b as char),
            _ => buf.push_str(&format!("\\{:03o}", b)),
        }
    }
    buf.push('"');
    buf
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn encode_c_string_good() {
        // Rust enforces UTF-8 validity
        let all_bytes = unsafe { String::from_utf8_unchecked((0..=255).collect()) };
        let tests = vec![
            "",
            "\"",
            "\"\"",
            "abc\"def",
            "\\",
            "\\\\",
            "\x0123abc", // trap here is if you encode '\x01' as "\\1"; it would join with the following digits: "\\123abc".
            "\n\r\t\x7f",
            "\\377",
            all_bytes.as_str(),
        ];

        for test in tests {
            let encoded = encode_c_string(&test);
            assert!(is_log_safe(&encoded));
        }
    }
}
