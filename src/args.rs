//! The `Args` datastructure is used throughout this crate to store client and server
//! configuration. This module implements the datastructure and several helper functions
//! to facilitate encoding and decoding formats into an `Args` structure.
use std::collections::HashMap;

use itertools::Itertools;

/// Key-value map used throughout the library to store client and server configuration.
///
/// Keys may have multiple values.
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Args(HashMap<String, Vec<String>>);

impl Args {
    /// Create an instance of `Args`, initialized with the provided values.
    pub fn new(args: HashMap<String, Vec<String>>) -> Self {
        Self(args)
    }

    pub fn get(&self, k: &str) -> Option<&Vec<String>> {
        self.0.get(k)
    }

    /// Get the first value associated with the given key.
    ///
    /// If the key is not set or the list of values is empty, returns `None`.
    fn get_first(&self, k: &str) -> Option<&String> {
        self.0.get(k)?.get(0)
    }

    /// Add a key and value into the datastructure.
    ///
    /// If the provided key already exists, add the value to the end of the values list for the
    /// provided key.
    pub fn add(&mut self, key: String, value: String) {
        self.0.entry(key).or_default().push(value);
    }

    /// Encode the arguments such that it can be used as the `ARGS` option of a `SMETHOD` line. The
    /// output is sorted by key.
    ///
    /// The characters `,`, `=`, and `\` are backslash escaped.
    pub(crate) fn encode_smethod_args(&self) -> String {
        const ESCAPE_CHARS: [char; 2] = [',', '='];
        self.0
            .iter()
            .flat_map(|(key, values)| {
                values.iter().map(move |v| {
                    format!(
                        "{}={}",
                        backslash_escape(key, &ESCAPE_CHARS),
                        backslash_escape(v, &ESCAPE_CHARS)
                    )
                })
            })
            .join(",")
    }
}

/// Errors that can be encountered when constructing the [`Args`] datastructure.
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum ArgsError {
    /// A zero-length key was found.
    EmptyKey,
    /// An escape sequence was started at the end of the string.
    IncompleteEscape,
    /// Could not find a complete key-value pair.
    InvalidKeyValuePair,
}

pub fn parse_client_parameters(mut s: &str) -> Result<Args, ArgsError> {
    let mut args = Args::default();

    while !s.is_empty() {
        let (key, c, rem) = split_unescaped(s, &['=', ';'])?;
        if key.is_empty() {
            return Err(ArgsError::EmptyKey);
        }
        if matches!(c, Some(';') | None) {
            return Err(ArgsError::InvalidKeyValuePair); // Found ';' or end of string before '='.
        }
        let (val, _c, rem) = split_unescaped(rem, &[';'])?;

        args.add(key, val);
        s = rem;
    }

    Ok(args)
}

/// Parse options in the form of `transport:name=value` eg, from `TOR_PT_SERVER_TRANSPORT_OPTIONS`.
///
/// For example: `scramblesuit:key=banana;automata:rule=110;automata:depth=3`.
pub fn parse_server_transport_options(mut s: &str) -> Result<HashMap<String, Args>, ArgsError> {
    let mut opts: HashMap<String, Args> = HashMap::new();

    while !s.is_empty() {
        let (method, c, rem) = split_unescaped(s, &[':', '=', ';'])?;
        if method.is_empty() {
            return Err(ArgsError::EmptyKey);
        }
        if matches!(c, Some('=') | Some(';') | None) {
            // Found '=', ';', or end of string before ':'.
            return Err(ArgsError::InvalidKeyValuePair);
        }

        let (key, c, rem) = split_unescaped(rem, &['=', ';'])?;
        if key.is_empty() {
            return Err(ArgsError::EmptyKey);
        }
        if matches!(c, Some(';') | None) {
            // Found ';' or end of string before '='.
            return Err(ArgsError::InvalidKeyValuePair);
        }
        let (val, _c, rem) = split_unescaped(rem, &[';'])?;

        opts.entry(method).or_default().add(key, val);
        s = rem;
    }

    Ok(opts)
}

/// Read the input, splitting at any of a given set of characters.
///
/// Read from input string `s`, un-escaping as characters are read. When a character from `chars`
/// is found, return the unescaped characters, the split character, and the remaining string.
///
/// Returns an error if an incomplete escape sequence is found (specifically, a '\' character at
/// the end of the string).
fn split_unescaped<'a>(
    s: &'a str,
    chars: &[char],
) -> Result<(String, Option<char>, &'a str), ArgsError> {
    // true if the previous character was the escape blackslash
    let mut escape = false;
    let mut buf = String::new();

    let split = s.chars().enumerate().find_map(|(i, c)| match (escape, c) {
        (true, _c) => {
            buf.push(c);
            escape = false;
            None
        }
        (false, '\\') => {
            escape = true;
            None
        }
        (false, c) => {
            if chars.contains(&c) {
                Some((c, &s[i + 1..]))
            } else {
                buf.push(c);
                None
            }
        }
    });

    if escape {
        // Escape sequence started at the end of the string.
        return Err(ArgsError::IncompleteEscape);
    }

    if let Some((c, rem)) = split {
        Ok((buf, Some(c), rem))
    } else {
        Ok((buf, None, ""))
    }
}

/// Backslash-escape all backslashes and characters provided in `chars`.
fn backslash_escape(s: &str, chars: &[char]) -> String {
    s.chars()
        .fold(String::with_capacity(s.len()), |mut res, c| {
            if c == '\\' {
                res.push_str("\\\\");
            } else if chars.contains(&c) {
                res.push('\\');
                res.push(c);
            } else {
                res.push(c);
            };
            res
        })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn args_encode_smethod_args_samples() {
        let tests = vec![
            (Args::default(), ""),
            (
                Args::new(hashmap! {
                    "j".into() => vec!["v1".into(), "v2".into(), "v3".into()],
                }),
                "j=v1,j=v2,j=v3",
            ),
            (
                Args::new(hashmap! {
                    "=,\\".into() => vec!["=".into(), ",".into(), "\\".into()],
                }),
                "\\=\\,\\\\=\\=,\\=\\,\\\\=\\,,\\=\\,\\\\=\\\\",
            ),
            (
                Args::new(hashmap! {
                    "secret".into() => vec!["yes".into()],
                }),
                "secret=yes",
            ),
            (
                Args::new(hashmap! {
                    "cache".into() => vec!["/tmp/cache".into()],
                }),
                "cache=/tmp/cache",
            ),
        ];

        for (args, expected) in tests {
            assert_eq!(args.encode_smethod_args(), expected);
        }
    }

    #[test]
    fn args_get_first() {
        let _args = Args::new(hashmap! {
            "a".into() => vec![],
            "b".into() => vec!["value".into()],
            "c".into() => vec!["v1".into(), "v2".into(), "v3".into()],
        });
    }

    #[test]
    fn parse_client_parameters_bad() {
        let tests = &[
            "key",
            "key\\",
            "=value",
            "==value",
            "==key=value",
            "key=value\\",
            "a;b=c",
            ";",
            // NOTE: This implementation differs from the Go implementation by allowing a semicolon
            // at the end of the client parameters. This does not appear to introduce any ambiguity
            // and appears to conform to the spec.
            // "key=value;",
            ";key=value",
            "key\\=value",
        ];

        for test in tests {
            println!("parsing: {}", test);
            assert!(parse_client_parameters(test).is_err());
        }
    }

    #[test]
    fn parse_client_parameters_good() {
        let tests = vec![
            ("", Args::default()),
            (
                "key=",
                Args::new(hashmap! {
                   "key".into() => vec!["".into()],
                }),
            ),
            (
                "key==",
                Args::new(hashmap! {
                    "key".into() => vec!["=".into()],
                }),
            ),
            (
                "key=value",
                Args::new(hashmap! {
                    "key".into() => vec!["value".into()],
                }),
            ),
            (
                "a=b=c",
                Args::new(hashmap! {
                    "a".into() => vec!["b=c".into()],
                }),
            ),
            (
                "a=bc==",
                Args::new(hashmap! {
                    "a".into() => vec!["bc==".into()],
                }),
            ),
            (
                "key=a\nb",
                Args::new(hashmap! {
                    "key".into() => vec!["a\nb".into()],
                }),
            ),
            (
                "key=value\\;",
                Args::new(hashmap! {
                    "key".into() => vec!["value;".into()],
                }),
            ),
            (
                "key=\"value\"",
                Args::new(hashmap! {
                    "key".into() => vec!["\"value\"".into()],
                }),
            ),
            (
                "key=\"\"value\"\"",
                Args::new(hashmap! {
                    "key".into() => vec!["\"\"value\"\"".into()]
                }),
            ),
            (
                "\"key=value\"",
                Args::new(hashmap! {
                    "\"key".into() => vec!["value\"".into()],
                }),
            ),
            (
                "key=value;key=value",
                Args::new(hashmap! {
                    "key".into() => vec!["value".into(), "value".into()],
                }),
            ),
            (
                "key=value1;key=value2",
                Args::new(hashmap! {
                    "key".into() => vec!["value1".into(), "value2".into()],
                }),
            ),
            (
                "key1=value1;key2=value2;key1=value3",
                Args::new(hashmap! {
                    "key1".into() => vec!["value1".into(), "value3".into()],
                    "key2".into() => vec!["value2".into()],
                }),
            ),
            (
                "\\;=\\;;\\\\=\\;",
                Args::new(hashmap! {
                    ";".into() => vec![";".into()],
                    "\\".into() => vec![";".into()],
                }),
            ),
            (
                "a\\=b=c",
                Args::new(hashmap! {
                    "a=b".into() => vec!["c".into()],
                }),
            ),
            (
                "shared-secret=rahasia;secrets-file=/tmp/blob",
                Args::new(hashmap! {
                    "shared-secret".into() => vec!["rahasia".into()],
                    "secrets-file".into() => vec!["/tmp/blob".into()],
                }),
            ),
            (
                "rocks=20;height=5.6",
                Args::new(hashmap! {
                    "rocks".into() => vec!["20".into()],
                    "height".into() => vec!["5.6".into()],
                }),
            ),
        ];

        for (input, expected) in tests {
            println!("parsing: {}", input);
            assert_eq!(parse_client_parameters(input).unwrap(), expected);
        }
    }

    #[test]
    fn parse_server_transport_options_bad() {
        let tests = &[
            "t\\",
            ":=",
            "t:=",
            ":k=",
            ":=v",
            "t:=v",
            "t:=v",
            "t:k\\",
            // NOTE: This implementation differs from the Go implementation by allowing a semicolon
            // at the end of the transport options. This does not appear to introduce any ambiguity
            // and appears to conform to the spec.
            // "t:k=v;",
            "abc",
            "t:",
            "key=value",
            "=value",
            "t:k=v\\",
            "t1:k=v;t2:k=v\\",
            "t:=key=value",
            "t:==key=value",
            "t:;key=value",
            "t:key\\=value",
        ];

        for test in tests {
            println!("parsing: {}", test);
            assert!(parse_server_transport_options(test).is_err());
        }
    }

    #[test]
    fn parse_server_transport_options_good() {
        let tests = vec![
            ("", HashMap::default()),
            (
                "t:k=v",
                hashmap! {
                    "t".into() => Args::new(hashmap! {
                        "k".into() => vec!["v".into()],
                    })
                },
            ),
            (
                "t:k=v=v",
                hashmap! {
                    "t".into() => Args::new(hashmap! {
                        "k".into() => vec!["v=v".into()],
                    }),
                },
            ),
            (
                "t:k=vv==",
                hashmap! {
                    "t".into() => Args::new(hashmap! {
                        "k".into() => vec!["vv==".into()],
                    }),
                },
            ),
            (
                "t1:k=v1;t2:k=v2;t1:k=v3",
                hashmap! {
                    "t1".into() => Args::new(hashmap! {
                        "k".into() => vec!["v1".into(), "v3".into()],
                    }),
                    "t2".into() => Args::new(hashmap! {
                        "k".into() => vec!["v2".into()],
                    }),
                },
            ),
            (
                "t\\:1:k=v;t\\=2:k=v;t\\;3:k=v;t\\\\4:k=v",
                hashmap! {
                    "t:1".into() => Args::new(hashmap! {
                        "k".into() => vec!["v".into()],
                    }),
                    "t=2".into() => Args::new(hashmap! {
                        "k".into() => vec!["v".into()],
                    }),
                    "t;3".into() => Args::new(hashmap! {
                        "k".into() => vec!["v".into()],
                    }),
                    "t\\4".into() => Args::new(hashmap! {
                        "k".into() => vec!["v".into()],
                    }),
                },
            ),
            (
                "t:k\\:1=v;t:k\\=2=v;t:k\\;3=v;t:k\\\\4=v",
                hashmap! {
                    "t".into() => Args::new(hashmap! {
                        "k:1".into() => vec!["v".into()],
                        "k=2".into() => vec!["v".into()],
                        "k;3".into() => vec!["v".into()],
                        "k\\4".into() => vec!["v".into()],
                    }),
                },
            ),
            (
                "t:k=v\\:1;t:k=v\\=2;t:k=v\\;3;t:k=v\\\\4",
                hashmap! {
                    "t".into() => Args::new(hashmap! {
                        "k".into() => vec!["v:1".into(), "v=2".into(), "v;3".into(), "v\\4".into()],
                    })
                },
            ),
            (
                "trebuchet:secret=nou;trebuchet:cache=/tmp/cache;ballista:secret=yes",
                hashmap! {
                    "trebuchet".into() => Args::new(hashmap! {
                        "secret".into() => vec!["nou".into()],
                        "cache".into() => vec!["/tmp/cache".into()],
                    }),
                    "ballista".into() => Args::new(hashmap! {
                        "secret".into() => vec!["yes".into()],
                    })
                },
            ),
        ];

        for (input, expected) in tests {
            println!("parsing: {}", input);
            assert_eq!(parse_server_transport_options(input).unwrap(), expected);
        }
    }

    #[test]
    fn split_unescaped_cases() {
        let tests = vec![
            ("j=v1", ("j".into(), Some('='), "v1")),
            ("j\\=v1", ("j=v1".into(), None, "")),
            ("j=v1;j=v2", ("j".into(), Some('='), "v1;j=v2")),
        ];

        for (input, expected) in tests {
            assert_eq!(split_unescaped(input, &['=', ';']).unwrap(), expected);
        }
    }
}
