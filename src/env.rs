//! Access pluggable transport configuration stored in environment variables.
use std::env::{self, VarError};
use std::ffi::OsStr;
use std::io;
use std::net::{AddrParseError, SocketAddr};
use std::{collections::HashSet, path::PathBuf};

use url::Url;

use crate::args::{self, ArgsError};

const VAR_AUTH_COOKIE_FILE: &str = "TOR_PT_AUTH_COOKIE_FILE";
const VAR_CLIENT_TRANSPORTS: &str = "TOR_PT_CLIENT_TRANSPORTS";
const VAR_EXTENDED_SERVER_PORT: &str = "TOR_PT_EXTENDED_SERVER_PORT";
const VAR_ORPORT: &str = "TOR_PT_ORPORT";
const VAR_PROXY: &str = "TOR_PT_PROXY";
const VAR_SERVER_BINDADDR: &str = "TOR_PT_SERVER_BINDADDR";
const VAR_SERVER_TRANSPORTS: &str = "TOR_PT_SERVER_TRANSPORTS";
const VAR_SERVER_TRANSPORT_OPTIONS: &str = "TOR_PT_SERVER_TRANSPORT_OPTIONS";
const VAR_STATE_LOCATION: &str = "TOR_PT_STATE_LOCATION";
const VAR_TRANSPORT_VER: &str = "TOR_PT_MANAGED_TRANSPORT_VER";
const SUPPORTED_TRANSPORT_VERSIONS: [&str; 1] = ["1"];

/// Read an environment variable.
///
/// This function behaves similarly to `std::env::var`, except it also returns
/// `VarError::NotPresent` if the variable exists but is an empty string.
fn var_required<K: AsRef<OsStr>>(key: K) -> Result<String, VarError> {
    let value = env::var(key)?;
    if value.is_empty() {
        Err(VarError::NotPresent)
    } else {
        Ok(value)
    }
}

pub fn auth_cookie_file() -> Option<PathBuf> {
    env::var(VAR_AUTH_COOKIE_FILE).ok().map(PathBuf::from)
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum TransportVersionError {
    /// No transport version was found in the `TOR_PT_MANAGED_TRANSPORT_VER` environment variable.
    VariableNotFound,
    /// None of the supplied transport versions were supported by this implementation.
    NoSupportedVersion,
}

/// Returns the first pluggable transport version supported by this implementation. Returns an
/// error if the version list is not provided or no supported version was found.
pub fn managed_transport_version() -> Result<String, TransportVersionError> {
    let ver = env::var(VAR_TRANSPORT_VER).or(Err(TransportVersionError::VariableNotFound))?;
    ver.split(',')
        .find(|offered| SUPPORTED_TRANSPORT_VERSIONS.contains(&offered))
        .map(String::from)
        .ok_or(TransportVersionError::NoSupportedVersion)
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum ClientTransportsError {
    VariableNotFound,
}

/// Returns the list of method names requested by Tor as configured in `TOR_PT_CLIENT_TRANSPORTS`.
pub fn client_transports() -> Result<Vec<String>, ClientTransportsError> {
    let transports =
        env::var(VAR_CLIENT_TRANSPORTS).or(Err(ClientTransportsError::VariableNotFound))?;
    Ok(transports.split(',').map(String::from).collect())
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum ProxyUrlError {
    /// The URL in `TOR_PT_PROXY` was missing a hostname.
    MissingHost,
    /// The URL in `TOR_PT_PROXY` was missing a port.
    MissingPort,
    /// The contents of the `TOR_PT_PROXY` environment variable were not valid unicode.
    UnicodeError,
    /// Could not parse the URL contained in `TOR_PT_PROXY`.
    UrlParseError(url::ParseError),
}
impl From<url::ParseError> for ProxyUrlError {
    fn from(err: url::ParseError) -> Self {
        Self::UrlParseError(err)
    }
}

/// Parse the upstream proxy URL from `TOR_PT_PROXY`. Returns `Ok(None)` if no proxy was
/// configured. This function checks for a missing host or port to ensure the URL is not relative.
/// The `url` crate enforces that a scheme must be provided.
///
/// This function does not check that the scheme is a supported proxy scheme; this is the caller's
/// responsibility.
pub fn proxy_url() -> Result<Option<Url>, ProxyUrlError> {
    let rawurl = match env::var(VAR_PROXY) {
        Ok(rawurl) => {
            if rawurl.is_empty() {
                return Ok(None);
            }
            rawurl
        }
        Err(VarError::NotPresent) => return Ok(None),
        Err(VarError::NotUnicode(_)) => return Err(ProxyUrlError::UnicodeError),
    };

    let u = Url::parse(&rawurl)?;
    validate_proxy_url(&u)?;

    Ok(Some(u))
}

pub(crate) fn validate_proxy_url(u: &Url) -> Result<(), ProxyUrlError> {
    if u.host().is_none() {
        return Err(ProxyUrlError::MissingHost);
    }
    if u.port().is_none() {
        return Err(ProxyUrlError::MissingPort);
    }
    Ok(())
}

pub fn resolve_addr(addr: &str) -> Result<SocketAddr, std::net::AddrParseError> {
    addr.parse()
}

pub fn extended_or_addr() -> Result<Option<SocketAddr>, std::net::AddrParseError> {
    match env::var(VAR_EXTENDED_SERVER_PORT) {
        Ok(addr) => {
            if addr.is_empty() {
                Ok(None)
            } else {
                Ok(Some(resolve_addr(&addr)?))
            }
        }
        Err(_) => Ok(None),
    }
}

pub fn or_addr() -> Result<Option<SocketAddr>, std::net::AddrParseError> {
    match env::var(VAR_ORPORT) {
        Ok(addr) => {
            if addr.is_empty() {
                Ok(None)
            } else {
                Ok(Some(resolve_addr(&addr)?))
            }
        }
        Err(_) => Ok(None),
    }
}

/// Method name, socket address as read from `TOR_PT_SERVER_BINDADDR`.
///
/// This struct also holds additional parameters
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct BindAddr {
    method: String,
    addr: SocketAddr,
    options: Option<args::Args>,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum BindAddrsError {
    /// Error parsing server transport options.
    ArgsError(ArgsError),
    /// Failed to parse an IP address/port.
    AddrParseError(AddrParseError),
    /// Found a duplicate method in the configuration.
    DuplicateMethod,
    /// Could not parse method and address.
    MethodParseError,
    /// A required environment was not found.
    VariableNotFound,
}
impl From<AddrParseError> for BindAddrsError {
    fn from(e: AddrParseError) -> Self {
        Self::AddrParseError(e)
    }
}
impl From<ArgsError> for BindAddrsError {
    fn from(e: ArgsError) -> Self {
        Self::ArgsError(e)
    }
}
impl From<VarError> for BindAddrsError {
    fn from(_e: VarError) -> Self {
        Self::VariableNotFound
    }
}

/// Join configurations from `TOR_PT_SERVER_BINDADDR`, `TOR_PT_SERVER_TRANSPORTS`, and
/// `TOR_PT_SERVER_TRANSPORT_OPTIONS` into a vector of [`BindAddr`] structs.
pub fn server_bind_addrs() -> Result<Vec<BindAddr>, BindAddrsError> {
    // Build map from method to `Args` from the transport options
    let transport_options =
        args::parse_server_transport_options(&env::var(VAR_SERVER_TRANSPORT_OPTIONS)?)?;

    // Parse individual bind addresses, erroring if a duplicate method is found
    let bindaddr_str = var_required(VAR_SERVER_BINDADDR)?;
    let mut seen_methods = HashSet::new();
    let mut res = bindaddr_str
        .split(',')
        .map(|spec| {
            let mut parts = spec.splitn(2, '-');
            let method: String = parts.next().ok_or(BindAddrsError::MethodParseError)?.into();
            let addr = resolve_addr(parts.next().ok_or(BindAddrsError::MethodParseError)?)?;
            let bind_options = transport_options.get(&method).cloned();

            if !seen_methods.insert(method.clone()) {
                // method was already in set
                return Err(BindAddrsError::DuplicateMethod);
            }

            Ok(BindAddr {
                method,
                addr,
                options: bind_options,
            })
        })
        .collect::<Result<Vec<BindAddr>, BindAddrsError>>()?;

    // Remove any BindAddr's for methods not configured in `TOR_PT_SERVER_TRANSPORTS`
    let configured_transports_str = var_required(VAR_SERVER_TRANSPORTS)?;
    let configured_transports: Vec<&str> = configured_transports_str.split(',').collect();
    res.retain(|bindaddr| configured_transports.contains(&bindaddr.method.as_str()));

    Ok(res)
}

#[derive(Debug)]
pub enum MakeStateDirError {
    IoError(io::Error),
    /// No state directory was found in the `TOR_PT_STATE_LOCATION` environment variable.
    VariableNotFound,
}
impl From<io::Error> for MakeStateDirError {
    fn from(err: io::Error) -> Self {
        Self::IoError(err)
    }
}

/// Return the path to the directory specified in `TOR_PT_STATE_LOCATION`, creating the directory
/// if it does not exist.
pub fn make_state_dir() -> Result<PathBuf, MakeStateDirError> {
    let path = var_required(VAR_STATE_LOCATION).or(Err(MakeStateDirError::VariableNotFound))?;
    std::fs::create_dir_all(&path)?;
    Ok(PathBuf::from(path))
}

#[cfg(test)]
mod tests {

    use std::lazy::SyncLazy;
    use std::net::{Ipv4Addr, Ipv6Addr, SocketAddrV4, SocketAddrV6};
    use std::sync::Mutex;

    use super::*;
    use crate::args::Args;

    static ENV_LOCK: SyncLazy<Mutex<()>> = SyncLazy::new(|| Mutex::new(()));

    #[test]
    fn resolve_addr_bad() {
        let tests = &[
            "",
            "1.2.3.4",
            "1.2.3.4:",
            "9999",
            ":9999",
            "[1:2::3:4]",
            "[1:2::3:4]:",
            "[1::2::3:4]",
            "1:2::3:4::9999",
            "1:2:3:4::9999",
            "localhost:9999",
            "[localhost]:9999",
            "1.2.3.4:http",
            "1.2.3.4:0x50",
            "1.2.3.4:-65456",
            "1.2.3.4:65536",
            "1.2.3.4:80\x00",
            "1.2.3.4:80 ",
            " 1.2.3.4:80",
            "1.2.3.4 : 80",
        ];

        for test in tests {
            assert!(resolve_addr(test).is_err());
        }
    }

    #[test]
    fn resolve_addr_good() {
        assert_eq!(
            resolve_addr("1.2.3.4:9999"),
            Ok(SocketAddr::V4(SocketAddrV4::new(
                Ipv4Addr::new(1, 2, 3, 4),
                9999
            )))
        );
        assert_eq!(
            resolve_addr("[1:2::3:4]:9999"),
            Ok(SocketAddr::V6(SocketAddrV6::new(
                Ipv6Addr::new(1, 2, 0, 0, 0, 0, 3, 4),
                9999,
                0,
                0
            )))
        );
        // NOTE: The Go implementation appears to have backwards compatibility for IPv6 addresses
        // represented without brackets. IPv6 addresses appear to have been represented with
        // brackets for multiple years, so the backwards compatibility has been removed for
        // simplicity.
        // https://bugs.torproject.org/7011
        // assert_eq!(
        //     resolve_addr("1:2::3:4:9999"),
        //     Ok(SocketAddr::V6(SocketAddrV6::new(
        //         Ipv6Addr::new(1, 2, 0, 0, 0, 0, 3, 4),
        //         9999,
        //         0,
        //         0
        //     )))
        // );
    }

    #[test]
    fn server_bind_addrs_bad() {
        let tests = &[
            // bad TOR_PT_SERVER_BINDADDR
            ("alpha", "alpha", ""),
            ("alpha-1.2.3.4", "alpha", ""),
            // missing TOR_PT_SERVER_TRANSPORTS
            ("alpha-1.2.3.4:1111", "", "alpha:key=value"),
            // bad TOR_PT_SERVER_TRANSPORT_OPTIONS
            ("alpha-1.2.3.4:1111", "alpha", "key=value"),
            // no escaping is defined for TOR_PT_SERVER_TRANSPORTS or
            // TOR_PT_SERVER_BINDADDR.
            ("alpha\\,beta-1.2.3.4:1111", "alpha\\,beta", ""),
            // duplicates in TOR_PT_SERVER_BINDADDR
            // https://bugs.torproject.org/21261
            ("alpha-0.0.0.0:1234,alpha-[::]:1234", "alpha", ""),
            ("alpha-0.0.0.0:1234,alpha-0.0.0.0:1234", "alpha", ""),
        ];

        for test in tests {
            let _guard = ENV_LOCK.lock();
            println!("testing environment variables: {:?}", test);
            env::set_var(VAR_SERVER_BINDADDR, test.0);
            env::set_var(VAR_SERVER_TRANSPORTS, test.1);
            env::set_var(VAR_SERVER_TRANSPORT_OPTIONS, test.2);

            assert!(server_bind_addrs().is_err());
        }
    }

    #[test]
    fn server_bind_addrs_good() {
        let tests = &[
            (
                "alpha-1.2.3.4:1111,beta-[1:2::3:4]:2222",
                "alpha,beta,gamma",
                "alpha:k1=v1;beta:k2=v2;gamma:k3=v3",
                vec![
                    BindAddr {
                        method: "alpha".into(),
                        addr: "1.2.3.4:1111".parse().unwrap(),
                        options: Some(Args::new(hashmap! {"k1".into() => vec!["v1".into()]})),
                    },
                    BindAddr {
                        method: "beta".into(),
                        addr: "[1:2::3:4]:2222".parse().unwrap(),
                        options: Some(Args::new(hashmap! {"k2".into() => vec!["v2".into()]})),
                    },
                ],
            ),
            ("alpha-1.2.3.4:1111", "xxx", "", vec![]),
            (
                "alpha-1.2.3.4:1111",
                "alpha,beta,gamma",
                "",
                vec![BindAddr {
                    method: "alpha".into(),
                    addr: "1.2.3.4:1111".parse().unwrap(),
                    options: None,
                }],
            ),
            (
                "trebuchet-127.0.0.1:1984,ballista-127.0.0.1:4891",
                "trebuchet,ballista",
                "trebuchet:secret=nou;trebuchet:cache=/tmp/cache;ballista:secret=yes",
                vec![
                    BindAddr {
                        method: "trebuchet".into(),
                        addr: "127.0.0.1:1984".parse().unwrap(),
                        options: Some(Args::new(hashmap! {
                            "secret".into() => vec!["nou".into()],
                            "cache".into() => vec!["/tmp/cache".into()],
                        })),
                    },
                    BindAddr {
                        method: "ballista".into(),
                        addr: "127.0.0.1:4891".parse().unwrap(),
                        options: Some(Args::new(hashmap! {
                            "secret".into() => vec!["yes".into()],
                        })),
                    },
                ],
            ),
        ];

        for test in tests {
            let _guard = ENV_LOCK.lock();
            println!("testing environment variables: {:?}", test);
            env::set_var(VAR_SERVER_BINDADDR, test.0);
            env::set_var(VAR_SERVER_TRANSPORTS, test.1);
            env::set_var(VAR_SERVER_TRANSPORT_OPTIONS, test.2);

            assert_eq!(server_bind_addrs(), Ok(test.3.clone()));
        }
    }
}
