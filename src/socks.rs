use std::net::{self, TcpStream, ToSocketAddrs};
use std::time::Duration;
use std::{
    io::{self, Read, Write},
    string::FromUtf8Error,
};

use crate::args::{self, Args, ArgsError};

const REQUEST_TIMEOUT: Duration = Duration::from_secs(5);
const SOCKS_VERSION: u8 = 0x05;

const SOCKS_AUTH_NONE_REQUIRED: u8 = 0x00;
const SOCKS_AUTH_USERNAME_PASSWORD: u8 = 0x02;
const SOCKS_AUTH_NO_ACCEPTABLE_METHODS: u8 = 0xff;

const SOCKS_AUTH_RFC1929_VERSION: u8 = 0x01;
const SOCKS_AUTH_RFC1929_SUCCESS: u8 = 0x00;
const SOCKS_AUTH_RFC1929_FAIL: u8 = 0x01;
const SOCKS_AUTH_ERROR_RESPONSE: [u8; 2] = [SOCKS_AUTH_RFC1929_VERSION, SOCKS_AUTH_RFC1929_FAIL];

struct SocksRequest {
    target: String,
    username: String,
    password: String,
    args: Args,
}

impl SocksRequest {
    fn set_password(&mut self, password: String) {
        self.password = password;
    }

    fn set_username(&mut self, username: String) {
        self.username = username;
    }
}

struct SocksConn {
    conn: net::TcpStream,
    req: SocksRequest,
}

impl SocksConn {
    /// Send a message to the client indicating that access to the provided addresses is granted.
    ///
    /// `addr` is ignored and `0.0.0.0:0` is always sent back for the bind address/port in the
    /// response.
    fn grant(addr: net::SocketAddr) -> Result<(), io::Error> {
        unimplemented!()
    }

    fn reject() -> Result<(), io::Error> {
        unimplemented!()
    }

    fn reject_reason(reason: &[u8]) -> Result<(), io::Error> {
        unimplemented!()
    }
}

struct SocksListener {
    listener: net::TcpListener,
}

impl SocksListener {
    fn new<A: ToSocketAddrs>(addr: A) -> Result<Self, io::Error> {
        Ok(Self {
            listener: net::TcpListener::bind(addr)?,
        })
    }

    fn accept(&self) -> Result<TcpStream, io::Error> {
        self.listener.accept().map(|(stream, addr)| stream)
    }

    fn accept_socks(&self) -> Result<SocksConn, io::Error> {
        let mut stream = self.accept()?;
        stream.set_read_timeout(Some(REQUEST_TIMEOUT))?;
        stream.set_write_timeout(Some(REQUEST_TIMEOUT))?;

        let request = socks_5_handshake(&mut stream)?;

        stream.set_read_timeout(None)?;
        stream.set_write_timeout(None)?;

        Ok(SocksConn {
            conn: stream,
            req: request,
        })
    }

    fn version(&self) -> &str {
        "socks5"
    }
}

fn socks_5_handshake<C: Read + Write>(mut conn: C) -> Result<SocksRequest, io::Error> {
    unimplemented!()
}

#[derive(Debug)]
enum SocksNegotiateError {
    ByteVerifyError(ByteVerifyError),
    IoError(io::Error),
}
impl From<io::Error> for SocksNegotiateError {
    fn from(err: io::Error) -> Self {
        Self::IoError(err)
    }
}
impl From<ByteVerifyError> for SocksNegotiateError {
    fn from(err: ByteVerifyError) -> Self {
        Self::ByteVerifyError(err)
    }
}

fn socks_negotiate_auth<C: Read + Write>(mut conn: C) -> Result<SocksMethod, SocksNegotiateError> {
    // Check the version for SOCKS5.
    socks_read_byte_verify(&mut conn, "version", SOCKS_VERSION)?;

    // Read the number of auth methods offered by the server.
    let n_methods = socks_read_byte(&mut conn)? as usize;

    // Read the auth methods offered by the server.
    let mut methods = vec![0; n_methods];
    conn.read_exact(&mut methods)?;

    // Choose our preferred auth method, preferring username/password auth over the "none
    // required" method.
    let mut method = SocksMethod::NoAcceptableMethod;
    for method_code in methods {
        match (method_code, &method) {
            // Only proceed without auth if other methods (i.e., username/password auth) are not
            // also configured.
            (SOCKS_AUTH_NONE_REQUIRED, SocksMethod::NoAcceptableMethod) => {
                method = SocksMethod::NoneRequired;
            }
            (SOCKS_AUTH_USERNAME_PASSWORD, _) => {
                method = SocksMethod::UsernamePassword;
            }
            _ => {}
        }
    }

    // Send the negotiated auth method.
    let msg = [SOCKS_VERSION, method as u8];
    conn.write_all(&msg)?;
    conn.flush()?;

    Ok(method)
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum SocksMethod {
    NoAcceptableMethod = 0xff,
    NoneRequired = 0x00,
    UsernamePassword = 0x02,
}

#[derive(Debug)]
enum SocksAuthenticateError {
    NegotiationError(SocksNegotiateError),
}
impl From<SocksNegotiateError> for SocksAuthenticateError {
    fn from(err: SocksNegotiateError) -> Self {
        Self::NegotiationError(err)
    }
}

fn socks_authenticate<C: Read + Write>(
    mut conn: C,
    method: SocksMethod,
    req: &SocksRequest,
) -> Result<(), io::Error> {
    match method {
        SocksMethod::NoneRequired => {}
        SocksMethod::UsernamePassword => {
            unimplemented!()
        }
        SocksMethod::NoAcceptableMethod => {
            unimplemented!()
        }
    }

    conn.flush()?;
    Ok(())
}

#[derive(Debug)]
enum Rfc1929Error {
    /// Arguments provided in the SOCKS username/password were not parsable.
    InvalidArguments(ArgsError),
    /// Received an unexpected byte when reading the auth version.
    InvalidAuthVersion(ByteVerifyError),
    /// Usernames with a length of zero are invalid.
    InvalidUsernameLength,
    /// Passwords with a length of zero are invalid.
    InvalidPasswordLength,
    IoError(io::Error),
    /// Arguments provided in the SOCKS username/password were not valid UTF8.
    FromUtf8Error(FromUtf8Error),
}
impl From<io::Error> for Rfc1929Error {
    fn from(err: io::Error) -> Self {
        Self::IoError(err)
    }
}
impl From<FromUtf8Error> for Rfc1929Error {
    fn from(err: FromUtf8Error) -> Self {
        Self::FromUtf8Error(err)
    }
}

/// Authenticate the client with RFC1929 (username/password authentication).
///
/// Any valid username/password is accepted. These fields are used as an out-of-band argument
/// passing mechanism for pluggable transports.
fn socks_auth_rfc1929<C: Read + Write>(
    mut conn: C,
    method: SocksMethod,
    req: &mut SocksRequest,
) -> Result<(), Rfc1929Error> {
    fn send_error_response<C: Write>(mut conn: C) -> Result<(), io::Error> {
        let msg = [SOCKS_AUTH_RFC1929_VERSION, SOCKS_AUTH_RFC1929_FAIL];
        conn.write_all(&msg)?;
        conn.flush()?;
        Ok(())
    }

    if let Err(err) = socks_read_byte_verify(&mut conn, "auth version", SOCKS_AUTH_RFC1929_VERSION)
    {
        send_error_response(&mut conn)?;
        return Err(Rfc1929Error::InvalidAuthVersion(err));
    }

    // Read the username.
    let username_len = socks_read_byte(&mut conn)? as usize;
    if username_len < 1 {
        send_error_response(&mut conn)?;
        return Err(Rfc1929Error::InvalidUsernameLength);
    }
    let mut username = vec![0; username_len];
    conn.read_exact(&mut username)?;
    match String::from_utf8(username) {
        Ok(username_string) => req.set_username(username_string),
        Err(err) => {
            send_error_response(&mut conn)?;
            return Err(Rfc1929Error::FromUtf8Error(err));
        }
    }

    // Read the password.
    let password_len = socks_read_byte(&mut conn)? as usize;
    if password_len < 1 {
        send_error_response(&mut conn)?;
        return Err(Rfc1929Error::InvalidPasswordLength);
    }
    let mut password = vec![0; password_len];
    conn.read_exact(&mut password)?;

    // The password will be the null byte if there are no arguments.
    if !(password_len == 1 && password[0] == 0) {
        match String::from_utf8(password) {
            Ok(password_string) => req.set_password(password_string),
            Err(err) => {
                send_error_response(&mut conn)?;
                return Err(Rfc1929Error::FromUtf8Error(err));
            }
        }
    }

    // Concatenate the username and password, then parse it as an argument string.
    let client_parameters = {
        let mut params = String::with_capacity(username_len + password_len);
        params.push_str(&req.username);
        params.push_str(&req.password);
        params
    };

    match args::parse_client_parameters(&client_parameters) {
        Ok(arguments) => {
            req.args = arguments;
        }
        Err(err) => {
            send_error_response(&mut conn)?;
            return Err(Rfc1929Error::InvalidArguments(err));
        }
    }
    
    let msg = [SOCKS_AUTH_RFC1929_VERSION, SOCKS_AUTH_RFC1929_SUCCESS];
    conn.write_all(&msg)?;

    Ok(())
}

#[inline]
fn socks_read_byte<C: Read>(mut conn: C) -> Result<u8, io::Error> {
    let mut buf = [0; 1];
    conn.read_exact(&mut buf)?;
    Ok(buf[0])
}

#[derive(Debug)]
enum ByteVerifyError {
    /// Did not receive the expected value.
    Expected {
        field: &'static str,
        expected: u8,
        actual: u8,
    },
    IoError(io::Error),
}
impl From<io::Error> for ByteVerifyError {
    fn from(err: io::Error) -> Self {
        Self::IoError(err)
    }
}

fn socks_read_byte_verify<C: Read>(
    conn: C,
    field: &'static str,
    expected: u8,
) -> Result<(), ByteVerifyError> {
    let actual = socks_read_byte(conn)?;
    if actual != expected {
        return Err(ByteVerifyError::Expected {
            field,
            expected,
            actual,
        });
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use std::io::Cursor;

    use super::*;

    /// Bi-directional buffer implementing `Read` and `Write`.
    ///
    /// Used for unit tests as an alternative to TCP connections.
    struct RwBuf {
        pub read: Cursor<Vec<u8>>,
        pub write: Vec<u8>,
    }
    impl RwBuf {
        pub fn new(read: Vec<u8>) -> Self {
            Self {
                read: Cursor::new(read),
                write: Vec::new(),
            }
        }
    }
    impl io::Read for RwBuf {
        fn read(&mut self, buf: &mut [u8]) -> Result<usize, io::Error> {
            self.read.read(buf)
        }
    }
    impl io::Write for RwBuf {
        fn write(&mut self, buf: &[u8]) -> Result<usize, io::Error> {
            self.write.write(buf)
        }
        fn flush(&mut self) -> Result<(), io::Error> {
            self.write.flush()
        }
    }

    /// Test the behavior of `socks_negotiate_auth` when an unsupported SOCKS version is sent.
    #[test]
    fn socks_negotiate_auth_invalid_version() {
        let mut rw = RwBuf::new(b"\x03\x01\x00".to_vec());
        let res = socks_negotiate_auth(&mut rw);
        let err = res.unwrap_err();

        assert!(matches!(
            err,
            SocksNegotiateError::ByteVerifyError(ByteVerifyError::Expected {
                field: "version",
                expected: SOCKS_VERSION,
                actual: 0x03
            })
        ));
    }

    /// Test the behavior of `socks_negotiate_auth` when no methods are offered by the server.
    #[test]
    fn socks_negotiate_auth_invalid_n_methods() {
        let mut rw = RwBuf::new(b"\x05\x00".to_vec());
        let res = socks_negotiate_auth(&mut rw);
        assert_eq!(res.unwrap(), SocksMethod::NoAcceptableMethod);
    }

    /// Test that `socks_negotiate_auth` selects the "none required" method when it is the only
    /// supported method.
    #[test]
    fn socks_negotiate_auth_none_required() {
        let mut rw = RwBuf::new(b"\x05\x01\x00".to_vec());
        let res = socks_negotiate_auth(&mut rw);
        assert_eq!(res.unwrap(), SocksMethod::NoneRequired);
    }

    /// Test that `socks_negotiate_auth` selects the "username/password" method when it is the
    /// only supported method.
    #[test]
    fn socks_negotiate_auth_username_password() {
        let mut rw = RwBuf::new(b"\x05\x01\x02".to_vec());
        let res = socks_negotiate_auth(&mut rw);
        assert_eq!(res.unwrap(), SocksMethod::UsernamePassword);
    }

    /// Test that `socks_negotiate_auth` returns "no acceptable method" when all methods offered
    /// by the server are unsupported.
    #[test]
    fn socks_negotiate_auth_unsupported_method() {
        let mut rw = RwBuf::new(b"\x05\x01\x01".to_vec());
        let res = socks_negotiate_auth(&mut rw);
        assert_eq!(res.unwrap(), SocksMethod::NoAcceptableMethod);
    }

    /// Test that username/password method is preferred over the "none required" method.
    #[test]
    fn socks_negotiate_auth_prefers_username_password() {
        let mut rw = RwBuf::new(b"\x05\x03\x00\x01\x02".to_vec());
        let res = socks_negotiate_auth(&mut rw);
        assert_eq!(res.unwrap(), SocksMethod::UsernamePassword);
    }

    /// Assert that `SOCKS_AUTH_ERROR_RESPONSE` has been written to the connection.
    fn expect_auth_error(buf: &RwBuf) {
        assert_eq!(buf.write, SOCKS_AUTH_ERROR_RESPONSE);
    }

    /// Test that unsupported username/password auth versions are rejected.
    #[test]
    fn socks_auth_rfc1929_invalid_version() {
        let mut rw = RwBuf::new(b"\x03\x05abcde\x05abcde".to_vec());
        let res = socks_auth_rfc1929(
            &mut rw,
            SocksMethod::UsernamePassword,
            &mut SocksRequest {
                target: "".into(),
                username: "".into(),
                password: "".into(),
                args: Args::default(),
            },
        );
        let err = res.unwrap_err();
        expect_auth_error(&mut rw);

        assert!(matches!(
            err,
            Rfc1929Error::InvalidAuthVersion(ByteVerifyError::Expected {
                field: "auth version",
                expected: SOCKS_AUTH_RFC1929_VERSION,
                actual: 0x03,
            })
        ));
    }

    /// Test that username/password authentication attempts are rejected if the username length
    /// is zero.
    #[test]
    fn socks_auth_rfc1929_invalid_username_length() {
        let mut rw = RwBuf::new(b"\x01\x00\x05abcde".to_vec());
        let res = socks_auth_rfc1929(
            &mut rw,
            SocksMethod::UsernamePassword,
            &mut SocksRequest {
                target: "".into(),
                username: "".into(),
                password: "".into(),
                args: Args::default(),
            },
        );
        let err = res.unwrap_err();
        expect_auth_error(&mut rw);

        assert!(matches!(err, Rfc1929Error::InvalidUsernameLength));
    }

    /// Test that username/password authentication attempts are rejected if the password length
    /// is zero.
    #[test]
    fn socks_auth_rfc1929_invalid_password_length() {
        let mut rw = RwBuf::new(b"\x01\x05abcde\x00".to_vec());
        let res = socks_auth_rfc1929(
            &mut rw,
            SocksMethod::UsernamePassword,
            &mut SocksRequest {
                target: "".into(),
                username: "".into(),
                password: "".into(),
                args: Args::default(),
            },
        );
        let err = res.unwrap_err();
        expect_auth_error(&mut rw);

        assert!(matches!(err, Rfc1929Error::InvalidPasswordLength));
    }

    /// Test that username/password authentication attempts are rejected if the username and/or
    /// password contain invalid/unparsable arguments.
    #[test]
    fn socks_auth_rfc1929_invalid_pt_args() {
        let mut rw = RwBuf::new(b"\x01\x05abcde\x05abcde".to_vec());
        let res = socks_auth_rfc1929(
            &mut rw,
            SocksMethod::UsernamePassword,
            &mut SocksRequest {
                target: "".into(),
                username: "".into(),
                password: "".into(),
                args: Args::default(),
            },
        );
        let err = res.unwrap_err();
        expect_auth_error(&mut rw);

        assert!(matches!(err, Rfc1929Error::InvalidArguments(_)));
    }

    /// Test that a valid username/password authentication attempt is successful.
    #[test]
    fn socks_auth_rfc1929_success() {
        let mut rw = RwBuf::new(b"\x01\x09key=value\x01\x00".to_vec());
        let mut socks_request = SocksRequest {
            target: "".into(),
            username: "".into(),
            password: "".into(),
            args: Args::default(),
        };
        let res = socks_auth_rfc1929(
            &mut rw,
            SocksMethod::UsernamePassword,
            &mut socks_request,
        );
        assert!(res.is_ok());
        assert_eq!(rw.write, &[0x01, 0x00]);
        println!("{:?}", socks_request.args.get("key"));
        assert!(socks_request.args.get("key").unwrap().contains(&"value".to_string()));
    }
}
