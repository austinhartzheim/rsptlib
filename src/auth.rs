use std::io::{self, Read, Write};
use std::path::Path;

use hmac::{Hmac, Mac, NewMac};
use ring::rand::{SecureRandom, SystemRandom};
use sha2::Sha256;
use subtle::ConstantTimeEq;

use crate::server::ServerInfo;

enum AuthenticateError {
    /// The server rejected the authentication attempt.
    AuthenticationRejected,
    /// An error was encountered during IO. This is likely due to a network error while reading
    /// or writing from a connection.
    IoError(io::Error),
    /// Over 255 AuthTypes were offered by the server.
    TooManyAuthTypes,
    /// The path to the auth cookie file was not configured in `TOR_PT_AUTH_COOKIE_FILE`.
    NoAuthCookie,
    /// None of the server's offered auth schemes are supported by this client.
    NoSupportedAuthType,
    /// Error obtaining random bytes.
    RandomError(ring::error::Unspecified),
    /// Error reading the auth cookie from the configured file.
    ReadAuthCookieError(ReadAuthCookieError),
}
impl From<io::Error> for AuthenticateError {
    fn from(err: io::Error) -> Self {
        Self::IoError(err)
    }
}
impl From<ring::error::Unspecified> for AuthenticateError {
    fn from(err: ring::error::Unspecified) -> Self {
        Self::RandomError(err)
    }
}
impl From<ReadAuthCookieError> for AuthenticateError {
    fn from(err: ReadAuthCookieError) -> Self {
        Self::ReadAuthCookieError(err)
    }
}

fn authenticate<C: Read + Write>(
    mut conn: C,
    server_info: &ServerInfo,
) -> Result<(), AuthenticateError> {
    // AuthTypes are represented as single-byte values in the range 1..=255, with the value 0
    // indicating the end of the list. (`217-ext-orport-auth.txt`, section 4.1.)
    let mut auth_types = vec![false; 256];

    // There are a maximum of 255 valid AuthTypes, so we stop reading beyond that point in case
    // the input is malformed.
    let mut i = 0;
    let mut auth_buf: [u8; 1] = [0];
    while i < 256 {
        conn.read_exact(&mut auth_buf)?;
        if auth_buf[0] == 0 {
            break;
        }
        auth_types[auth_buf[0] as usize] = true;
        i += 1;
    }
    if i >= 256 {
        return Err(AuthenticateError::TooManyAuthTypes);
    }

    // Currently, we only support type 1 (`SAFE_COOKIE`).
    if !auth_types[1] {
        return Err(AuthenticateError::NoSupportedAuthType);
    }
    conn.write_all(&[1])?;

    // Generate a random client nonce and read the server hash and nonce from the connection.
    let mut client_nonce = [0u8; 32];
    let mut server_nonce = [0u8; 32];
    let mut server_hash = [0u8; 32];

    let random = SystemRandom::new();
    random.fill(&mut client_nonce)?;
    conn.write_all(&client_nonce)?;

    conn.read_exact(&mut server_hash)?;
    conn.read_exact(&mut server_nonce)?;

    let auth_cookie = read_auth_cookie_file(
        server_info
            .auth_cookie_path
            .as_ref()
            .ok_or(AuthenticateError::NoAuthCookie)?,
    )?;

    let expected_server_hash = compute_server_hash(&auth_cookie, &client_nonce, &server_nonce);
    let client_hash = compute_client_hash(&auth_cookie, &client_nonce, &server_nonce);
    conn.write_all(&client_hash)?;

    let mut status = [0; 1];
    conn.read_exact(&mut status)?;
    if status[0] != 1 {}

    Ok(())
}

pub enum ReadAuthCookieError {
    IoError(io::Error),
    InvalidLength,
    MissingCookieHeader,
}
impl From<io::Error> for ReadAuthCookieError {
    fn from(err: io::Error) -> Self {
        Self::IoError(err)
    }
}

/// Read an auth cookie from a type implementing `io::Read`, returning a 32-byte cookie. Returns
/// an error if the input is not 64 bytes or the header is invalid.
fn read_auth_cookie<R: io::Read>(mut reader: R) -> Result<Vec<u8>, ReadAuthCookieError> {
    static AUTH_HEADER: &[u8] = b"! Extended ORPort Auth Cookie !\x0a";

    let mut header = Vec::with_capacity(32);
    let mut cookie = Vec::with_capacity(32);

    reader.read_exact(&mut header)?;
    reader.read_exact(&mut cookie)?;
    if reader.bytes().next().is_some() {
        // Reader should have exactly 64 bytes, so the next read should fail.
        return Err(ReadAuthCookieError::InvalidLength);
    }

    // Check that the read header matches the expected `AUTH_HEADER`
    if bool::from(header.ct_eq(AUTH_HEADER)) {
        Ok(cookie)
    } else {
        Err(ReadAuthCookieError::MissingCookieHeader)
    }
}

pub fn read_auth_cookie_file<P: AsRef<Path>>(path: P) -> Result<Vec<u8>, ReadAuthCookieError> {
    let f = std::fs::File::open(path)?;
    read_auth_cookie(f)
}

fn compute_client_hash(auth_cookie: &[u8], client_nonce: &[u8], server_nonce: &[u8]) -> Vec<u8> {
    let mut h = Hmac::<Sha256>::new_varkey(auth_cookie).expect("Invalid HMAC key length");
    h.update(b"ExtORPort authentication client-to-server hash");
    h.update(client_nonce);
    h.update(server_nonce);
    h.finalize().into_bytes().to_vec()
}

pub fn compute_server_hash(
    auth_cookie: &[u8],
    client_nonce: &[u8],
    server_nonce: &[u8],
) -> Vec<u8> {
    let mut h = Hmac::<Sha256>::new_varkey(auth_cookie).expect("Invalid HMAC key length");
    h.update(b"ExtORPort authentication server-to-client hash");
    h.update(client_nonce);
    h.update(server_nonce);
    h.finalize().into_bytes().to_vec()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn compute_client_hash_sample() {
        let expected = b"\x0f\x36\x8b\x1b\xee\x24\xaa\xbc\x54\xa9\x11\x4c\xe0\x6c\x07\x0f\x3e\xd9\x9d\x0d\x36\x8f\x59\x9c\xcc\x6d\xfd\xc8\xbf\x45\x7a\x62";
        let auth_cookie = vec![0u8; 32];
        let client_nonce = vec![0u8; 32];
        let server_nonce = vec![0u8; 32];
        assert_eq!(
            compute_client_hash(&auth_cookie, &client_nonce, &server_nonce),
            expected
        );
    }

    #[test]
    fn compute_server_hash_sample() {
        let expected = b"\x9e\x22\x19\x19\x98\x2a\x84\xf7\x5f\xaf\x60\xef\x92\x69\x49\x79\x62\x68\xc9\x78\x33\xe0\x69\x60\xff\x26\x53\x69\xa9\x0f\xd6\xd8";
        let auth_cookie = vec![0u8; 32];
        let client_nonce = vec![0u8; 32];
        let server_nonce = vec![0u8; 32];
        assert_eq!(
            compute_server_hash(&auth_cookie, &client_nonce, &server_nonce),
            expected
        );
    }
}
