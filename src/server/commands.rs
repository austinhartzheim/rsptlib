use std::io::{self, Read, Write};

use super::ServerInfo;

pub const CMD_DONE: u16 = 0x0000;
pub const CMD_USER_ADDR: u16 = 0x0001;
pub const CMD_TRANSPORT: u16 = 0x0002;
pub const CMD_OKAY: u16 = 0x1000;
pub const CMD_DENY: u16 = 0x1001;

#[derive(Debug)]
pub enum SendCommandError {
    BodyTooLarge,
    IoError(io::Error),
}
impl From<io::Error> for SendCommandError {
    fn from(err: io::Error) -> Self {
        Self::IoError(err)
    }
}

fn send_command<W: Write>(mut writer: W, cmd: u16, body: &[u8]) -> Result<(), SendCommandError> {
    if body.len() > u16::MAX as usize {
        return Err(SendCommandError::BodyTooLarge);
    }

    writer.write_all(&cmd.to_be_bytes())?;
    writer.write_all(&(body.len() as u16).to_be_bytes())?;
    writer.write_all(body)?;

    Ok(())
}

fn send_user_addr<W: Write>(writer: W, addr: &str) -> Result<(), SendCommandError> {
    send_command(writer, CMD_USER_ADDR, addr.as_bytes())
}
fn send_transport<W: Write>(writer: W, method: &str) -> Result<(), SendCommandError> {
    send_command(writer, CMD_TRANSPORT, method.as_bytes())
}
fn send_done<W: Write>(writer: W) -> Result<(), SendCommandError> {
    send_command(writer, CMD_DONE, &[])
}

fn recv_command<R: Read>(mut reader: R) -> Result<(u16, Vec<u8>), io::Error> {
    let mut cmd = [0u8; 4];
    reader.read_exact(&mut cmd)?;

    let command = u16::from_be_bytes([cmd[0], cmd[1]]);
    let payload_len = u16::from_be_bytes([cmd[2], cmd[3]]);
    let mut buf = vec![0u8; payload_len as usize];

    reader.read_exact(&mut buf)?;
    Ok((command, buf))
}

#[derive(Debug)]
pub enum SetMetadataError {
    /// Error reading response from the server.
    IoError(io::Error),
    /// Error sending the commands required to set the metadata.
    SendError(SendCommandError),
    /// The server responded with `DENY`.
    SeverDenied,
    /// The server returned an unknown command in response to our metadata update.
    ServerUnknownCommand,
}
impl From<io::Error> for SetMetadataError {
    fn from(err: io::Error) -> Self {
        Self::IoError(err)
    }
}
impl From<SendCommandError> for SetMetadataError {
    fn from(err: SendCommandError) -> Self {
        Self::SendError(err)
    }
}

pub fn set_metadata<C: Read + Write>(
    mut connection: C,
    addr: &str,
    method: &str,
) -> Result<(), SetMetadataError> {
    if !addr.is_empty() {
        send_user_addr(&mut connection, addr)?;
    }
    if !method.is_empty() {
        send_transport(&mut connection, method)?;
    }
    send_done(&mut connection)?;

    let (cmd, _payload) = recv_command(&mut connection)?;
    match cmd {
        CMD_DENY => Err(SetMetadataError::SeverDenied),
        CMD_OKAY => Ok(()),
        _ => Err(SetMetadataError::ServerUnknownCommand),
    }
}

#[cfg(test)]
mod tests {
    use std::io::Cursor;
    use std::net::SocketAddr;

    use super::*;
    use crate::env;

    #[test]
    fn send_command_bad() {
        let tests = &[(0x0, vec![0; 65536]), (0x1234, vec![0; 65536])];

        for test in tests {
            let mut buf = Vec::new();
            send_command(&mut buf, test.0, &test.1)
                .expect_err("Successfully sent an invalid command");
        }
    }

    #[test]
    fn send_command_good() {
        let long_body = {
            let mut body = [0; 65535 + 2 + 2];
            body[0] = 0x12;
            body[1] = 0x34;
            body[2] = 0xff;
            body[3] = 0xff;
            body
        };
        let tests = &[
            (0x0, Vec::new(), vec![0u8; 4]),
            (0x5, Vec::new(), b"\x00\x05\x00\x00".to_vec()),
            (0xfffe, Vec::new(), b"\xff\xfe\x00\x00".to_vec()),
            (0xffff, Vec::new(), b"\xff\xff\x00\x00".to_vec()),
            (0x1234, b"hello".to_vec(), b"\x12\x34\x00\x05hello".to_vec()),
            (0x1234, vec![0; 65535].into(), long_body.to_vec()),
        ];

        for test in tests {
            let mut buf = Vec::new();
            send_command(&mut buf, test.0, &test.1).expect("Failed to send command");
            assert_eq!(buf, test.2);
        }
    }

    #[test]
    fn send_user_addr_good() {
        let addrs = &[
            "0.0.0.0:0",
            "1.2.3.4:9999",
            "255.255.255.255:65535",
            "[::]:0",
            "[ffff:ffff:ffff:ffff:ffff:ffff:255.255.255.255]:63335",
        ];

        for addr in addrs {
            let expected_addr: SocketAddr = addr.parse().unwrap();
            let mut buf = Vec::new();

            // Write command into buffer
            send_user_addr(&mut buf, addr).expect("Failed to send user address");
            assert!(
                buf.len() > 4,
                "Output was too short to have command and payload"
            );

            // Check buffer contents
            let cmd = u16::from_be_bytes([buf[0], buf[1]]);
            assert_eq!(cmd, CMD_USER_ADDR);
            let payload_len = u16::from_be_bytes([buf[2], buf[3]]);
            assert_eq!(
                buf.len(),
                payload_len as usize + 4,
                "Bytes written did not match length"
            );

            let addr_string =
                String::from_utf8(buf[4..].to_vec()).expect("Payload was not valid UTF8");
            let parsed_addr =
                env::resolve_addr(&addr_string).expect("Failed to parse written address");
            assert_eq!(expected_addr, parsed_addr);
        }
    }

    #[test]
    fn send_transport_good() {
        let tests = &[
            ("", b"\x00\x02\x00\x00".to_vec()),
            ("a", b"\x00\x02\x00\x01a".to_vec()),
            ("alpha", b"\x00\x02\x00\x05alpha".to_vec()),
        ];

        for (method, expected) in tests {
            let mut buf = Vec::new();
            send_transport(&mut buf, method).expect("Failed to send transport command");
            assert_eq!(&buf, expected);
        }
    }

    #[test]
    fn send_done_good() {
        let expected = b"\x00\x00\x00\x00".to_vec();

        let mut buf = Vec::new();
        send_done(&mut buf).expect("Failed to send done command");
        assert_eq!(buf, expected);
    }

    #[test]
    fn recv_command_bad() {
        let tests = &[
            b"".to_vec(),
            b"\x12".to_vec(),
            b"\x12\x34".to_vec(),
            b"\x12\x34\x00".to_vec(),
            b"\x12\x34\x00\x01".to_vec(),
        ];

        for input in tests {
            recv_command(Cursor::new(input)).expect_err("Successfully parsed an invalid command.");
        }
    }

    #[test]
    fn recv_command_good() {
        let tests = &[
            (
                b"\x12\x34\x00\x00".to_vec(),
                0x1234,
                b"".to_vec(),
                b"".to_vec(),
            ),
            (
                b"\x12\x34\x00\x00more".to_vec(),
                0x1234,
                b"".to_vec(),
                b"more".to_vec(),
            ),
            (
                b"\x12\x34\x00\x04body".to_vec(),
                0x1234,
                b"body".to_vec(),
                b"".to_vec(),
            ),
            (
                b"\x12\x34\x00\x04bodymore".to_vec(),
                0x1234,
                b"body".to_vec(),
                b"more".to_vec(),
            ),
        ];

        for (input, expected_cmd, expected_body, leftover) in tests {
            let mut cursor = Cursor::new(input);
            let (cmd, body) = recv_command(&mut cursor).expect("Failed to read valid command");
            assert_eq!(cmd, *expected_cmd);
            assert_eq!(body, *expected_body);

            let mut buf = Vec::new();
            cursor.read_to_end(&mut buf).unwrap();
            assert_eq!(buf, *leftover);
        }
    }

    /// Bi-directional buffer implementing `Read` and `Write`.
    ///
    /// Used for unit tests as an alternative to TCP connections.
    struct RwBuf {
        pub read: Cursor<Vec<u8>>,
        pub write: Vec<u8>,
    }
    impl RwBuf {
        pub fn new(read: Vec<u8>) -> Self {
            Self {
                read: Cursor::new(read),
                write: Vec::new(),
            }
        }
    }
    impl io::Read for RwBuf {
        fn read(&mut self, buf: &mut [u8]) -> Result<usize, io::Error> {
            self.read.read(buf)
        }
    }
    impl io::Write for RwBuf {
        fn write(&mut self, buf: &[u8]) -> Result<usize, io::Error> {
            self.write.write(buf)
        }
        fn flush(&mut self) -> Result<(), io::Error> {
            self.write.flush()
        }
    }

    fn test_set_metadata_individual(addr: &str, method: &str) {
        let mut ok_buf = Vec::new();
        send_command(&mut ok_buf, CMD_OKAY, &[]).unwrap();
        let mut rwbuf = RwBuf::new(ok_buf);

        set_metadata(&mut rwbuf, addr, method).expect("Failed to set metadata");

        // Check that the data we've written can be read by our parsers.
        let mut buf = Cursor::new(rwbuf.write);
        loop {
            let (cmd, body) = recv_command(&mut buf).expect("Failed reading set metadata commands");
            let body_str = String::from_utf8(body).expect("Body was not valid utf8");

            match cmd {
                CMD_DONE => break,
                CMD_USER_ADDR => assert_eq!(body_str, addr),
                CMD_TRANSPORT => assert_eq!(body_str, method),
                _ => panic!("Found unexpected command"),
            }
        }
    }

    #[test]
    fn set_metadata_good() {
        const ADDR: &str = "127.0.0.1:40000";
        const METHOD: &str = "alpha";

        test_set_metadata_individual("", "");
        test_set_metadata_individual(ADDR, "");
        test_set_metadata_individual("", METHOD);
        test_set_metadata_individual(ADDR, METHOD);
    }
}
