//! Provides the `ServerInfo` struct and other utilities for the implementation of pluggable
//! transport servers.
use std::net;
use std::path::PathBuf;

use crate::env::{self, BindAddr};

mod commands;

pub enum ServerInfoError {
    /// When `TOR_PT_EXTENDED_SERVER_PORT` is configured, `TOR_PT_AUTH_COOKIE_FILE` must also be
    /// provided.
    ExtendedOrPortRequiresAuthCookie,
    /// An error was encountered validating the configured bind addresses.
    InvalidBindAddrs(env::BindAddrsError),
    InvalidExtendedOrPortAddr(net::AddrParseError),
    InvalidOrPortAddr(net::AddrParseError),
    InvalidTransportVersion(env::TransportVersionError),
    /// Either the `TOR_PT_ORPORT` or `TOR_PT_EXTENDED_SERVER_PORT` environment variable must be
    /// configured.
    OrPortRequired,
}

pub struct ServerInfo {
    bind_addrs: Vec<BindAddr>,
    or_addr: Option<net::SocketAddr>,
    extended_or_addr: Option<net::SocketAddr>,
    pub auth_cookie_path: Option<PathBuf>,
}

impl ServerInfo {
    pub fn new() -> Result<ServerInfo, ServerInfoError> {
        let _version =
            env::managed_transport_version().map_err(ServerInfoError::InvalidTransportVersion)?;
        // TODO: log "VERSION version"

        let bind_addrs = env::server_bind_addrs().map_err(ServerInfoError::InvalidBindAddrs)?;
        let or_addr = env::or_addr().map_err(ServerInfoError::InvalidOrPortAddr)?;
        let auth_cookie_path = env::auth_cookie_file();
        let extended_or_addr = {
            let addr =
                env::extended_or_addr().map_err(ServerInfoError::InvalidExtendedOrPortAddr)?;
            match (addr, &auth_cookie_path) {
                (Some(addr), Some(_cookie_path)) => Some(addr),
                (Some(_addr), None) => {
                    // TODO: log that cookie path is required
                    return Err(ServerInfoError::ExtendedOrPortRequiresAuthCookie);
                }
                (None, _) => None,
            }
        };

        if or_addr.is_none() && extended_or_addr.is_none() {
            return Err(ServerInfoError::OrPortRequired);
        }

        Ok(ServerInfo {
            bind_addrs,
            or_addr,
            extended_or_addr,
            auth_cookie_path,
        })
    }
}
